Dieses Plugin zeigt nach dem Login ein der Benutzergruppe angepasstes Schnellmenü an.

Funktionen:
* Allgemeine Direktlinks Frontend-Login
* Gruppenbasierte Direktlinks Frontend-Login
* Gruppenbasierte Direktlinks Administrator-Login