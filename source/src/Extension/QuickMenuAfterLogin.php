<?php

/**
 * @package     Joomla.Plugin
 * @subpackage  Authentication.joomla
 *
 * @copyright   (C) 2006 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Joomla\Plugin\Authentication\QuickMenuAfterLogin\Extension;

use Joomla\CMS\Authentication\Authentication;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\User\User;
use Joomla\CMS\User\UserHelper;
use Joomla\Database\DatabaseAwareTrait;
use Joomla\CMS\Factory;

// phpcs:disable PSR1.Files.SideEffects
\defined('_JEXEC') or die;
// phpcs:enable PSR1.Files.SideEffects

/**
 * Joomla Authentication plugin
 *
 * @since  1.5
 */
final class QuickMenuAfterLogin extends CMSPlugin
{
    use DatabaseAwareTrait;


	/**
	 * @param $user
	 * @param $options
	 *
	 * @since 1.0.0
	 */
	public function onUserLogin($user, $options = array()) {

		if(!defined('GM_FRAMEWORK') or version_compare(constant('GM_FRAMEWORK'), '0.0.4') < 0) {
			if(defined('GM_FRAMEWORK')) {
				$Aufgabe = 'aktualisieren';
			} else {
				$Aufgabe = 'installieren oder <a href="https://docs.joomla.org/Help4.x:Plugins:_Name_of_Plugin/de" title="zur Joomla-Hilfeseite" target="_blank">aktivieren</a>';
			}

			exit('Bitte <a href="https://gitlab.com/gratia-mira-opensource/gratia-mira-framework/-/releases" title="zum Framework" target="_blank">GMFramework' . $Aufgabe . '</a>!');
		}

		$userid = UserHelper::getUserId($user['username']);
		$Benutzer = Factory::getUser($userid);

		// Ausgabe
		$msg = '<h2>Willkommen :)</h2>';
		$msg .= 'Hier finden ausgewählte Direktlinks:';
		$msg .= '<ol>';

		if(str_contains($options['entry_url'],'administrator')) {
			foreach ($this->params->get('LinksAdmin') as $Gruppe) {
				if($this->testeGruppen($Benutzer->groups,$Gruppe->ErlaubteGruppen)) {
					foreach ($Gruppe->LinksAllgemein as $Link) {
						$msg .= '<li>' . \GMF_Layout::erstelleLink($Link->UrlLink,$Link->NameLink,$Link->TitleLink) . '</li>';
					}
				}
			}

		} else {
			foreach ($this->params->get('LinksBenutzergruppen') as $Gruppe) {
				if($this->testeGruppen($Benutzer->groups,$Gruppe->ErlaubteGruppen)) {
					foreach ($Gruppe->LinksAllgemein as $Link) {
						$msg .= '<li>' . \GMF_Layout::erstelleLink($Link->UrlLink,$Link->NameLink,$Link->TitleLink) . '</li>';
					}
				}
			}

			foreach ($this->params->get('LinksAllgemein') as $Link) {
			   $msg .= '<li>' . \GMF_Layout::erstelleLink($Link->UrlLink,$Link->NameLink,$Link->TitleLink) . '</li>';
		   }
		}

		$msg .= '</ol>';


//		exit($msg);
	    Factory::getApplication()->enqueueMessage($msg, 'notice');
    }

	/**
	 * @param   array  $Benutzer
	 * @param   array  $Zielgruppen
	 *
	 * @return bool
	 *
	 * @since 1.0.0
	 */
	function testeGruppen(array $Benutzer, array $Zielgruppen):bool {
		foreach ($Benutzer as $Gruppe) {
			if(in_array($Gruppe,$Zielgruppen)) {
				return true;
			}
		}
		return false;
	}
}
